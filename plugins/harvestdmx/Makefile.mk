# LIBRARIES
##################################################
if USE_HARVESTDMX
lib_LTLIBRARIES += plugins/harvestdmx/libolaharvestdmx.la
plugins_harvestdmx_libolaharvestdmx_la_SOURCES = \
    plugins/harvestdmx/harvestdmxDevice.cpp \
    plugins/harvestdmx/harvestdmxDevice.h \
    plugins/harvestdmx/harvestdmxPlugin.cpp \
    plugins/harvestdmx/harvestdmxPlugin.h \
    plugins/harvestdmx/harvestdmxPort.h \
    plugins/harvestdmx/harvestdmxThread.cpp \
    plugins/harvestdmx/harvestdmxThread.h \
    plugins/harvestdmx/harvestdmxWidget.cpp \
    plugins/harvestdmx/harvestdmxWidget.h
plugins_harvestdmx_libolaharvestdmx_la_LIBADD = common/libolacommon.la
endif

