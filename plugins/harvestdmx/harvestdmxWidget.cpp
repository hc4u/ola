/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * This class is based on QLCFTDI class from
 *
 * Q Light Controller
 * qlcftdi-libftdi.cpp
 *
 * Copyright (C) Heikki Junnila
 *
 * Only standard CPP conversion was changed and function name changed
 * to follow OLA coding standards.
 *
 * by Rui Barreiros
 * Copyright (C) 2014 Richard Ash
 */

#include <strings.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
//#include <stropts.h>
#include <fcntl.h>
#include <termios.h>
//#include <asm/termios.h>
#include <unistd.h>

#include <string>
#include <algorithm>
#include <vector>

#include "ola/Constants.h"
#include "ola/io/ExtendedSerial.h"
#include "ola/io/IOUtils.h"
#include "ola/Logging.h"
#include "plugins/harvestdmx/harvestdmxWidget.h"

namespace ola {
namespace plugin {
namespace harvestdmx {

using std::string;
using std::vector;

harvestdmxWidget::harvestdmxWidget(const string& path)
    : m_path(path),
      m_fd(NOT_OPEN),
      m_channels(DMX_UNIVERSE_SIZE) {
}

harvestdmxWidget::~harvestdmxWidget() {
  if (IsOpen())
    Close();
}


bool harvestdmxWidget::Open() {
  OLA_DEBUG << "Opening serial port " << Name();
  if (!ola::io::Open(m_path, O_WRONLY, &m_fd)) {
    m_fd = FAILED_OPEN;
    OLA_WARN << Name() << " failed to open";
    return false;
  } else {
    OLA_DEBUG << "Opened serial port " << Name();
    return true;
  }
}

bool harvestdmxWidget::Close() {
  if (!IsOpen()) {
    return true;
  }

  {
    unsigned char buffer[2];

    buffer[0] = 'E';
    buffer[1] = 'V';

    if (write(m_fd, buffer, 2) <= 0) {
      // TODO(richardash1981): handle errors better as per the test code,
      // especially if we alter the scheduling!
      OLA_WARN << Name() << " Short or failed write!";
//      return false;
    }
  }

  if (close(m_fd) > 0) {
    OLA_WARN << Name() << " error closing";
    m_fd = NOT_OPEN;
    return false;
  } else {
    m_fd = NOT_OPEN;
    return true;
  }
}

bool harvestdmxWidget::IsOpen() const {
  return m_fd >= 0;
}

bool harvestdmxWidget::SetBreak(bool on) {
  unsigned long request;  /* NOLINT(runtime/int) */
  /* this is passed to ioctl, which is declared to take
   * unsigned long as it's second argument
   */
  if (on == true)
    request = TIOCSBRK;
  else
    request = TIOCCBRK;

  if (ioctl(m_fd, request, NULL) < 0) {
    OLA_WARN << Name() << " ioctl() failed";
    return false;
  } else {
    return true;
  }
}

unsigned char nybbleTable[] = "0123456789abcdef";
bool harvestdmxWidget::SetMax(const int dmxChannels) {
  unsigned char buffer[4];

  m_channels = dmxChannels;

  buffer[0] = 'M';
  buffer[1] = nybbleTable[(dmxChannels >> 8) & 0xF];
  buffer[2] = nybbleTable[(dmxChannels >> 4) & 0xF];
  buffer[3] = nybbleTable[(dmxChannels >> 0) & 0xF];

  if (write(m_fd, buffer, 4) <= 0) {
    // TODO(richardash1981): handle errors better as per the test code,
    // especially if we alter the scheduling!
    OLA_WARN << Name() << " Short or failed write!";
    return false;
  } else {
    return true;
  }
}

bool harvestdmxWidget::Write(const ola::DmxBuffer& data) {
  unsigned char ibuffer[DMX_UNIVERSE_SIZE];
  unsigned char obuffer[DMX_UNIVERSE_SIZE*2 + 1];
  unsigned int length = m_channels;
  int x;

  data.Get(ibuffer, &length);

  if(length > 0) {
    length = m_channels;

    // Nybblize the dmx data for transmission.
    for(x = 0; x < m_channels; x++) {
      unsigned char t = ibuffer[x];
      obuffer[x*2+1] = nybbleTable[(t >> 4) & 0xF];
      obuffer[x*2+2] = nybbleTable[(t >> 0) & 0xF];
    }
    obuffer[0] = 'T';

    if (write(m_fd, obuffer, length*2 + 1) <= 0) {
      // TODO(richardash1981): handle errors better as per the test code,
      // especially if we alter the scheduling!
      OLA_WARN << Name() << " Short or failed write!";
      return false;
    }
  }
  return true;
}

bool harvestdmxWidget::Read(unsigned char *buff, int size) {
  int readb = read(m_fd, buff, size);
  if (readb <= 0) {
    OLA_WARN << Name() << " read error";
    return false;
  } else {
    return true;
  }
}

/**
 * Setup our device for DMX send
 * Also used to test if device is working correctly
 * before AddDevice()
 */
bool harvestdmxWidget::SetupOutput() {
  struct termios my_tios;
  // Setup the Arduino for DMX
  if (Open() == false) {
    OLA_WARN << "Error Opening widget";
    return false;
  }
  /* do the port settings */

  if (tcgetattr(m_fd, &my_tios) < 0) {  // get current settings
    OLA_WARN << "Failed to get POSIX port settings";
    return false;
  }
  cfmakeraw(&my_tios);  // make it a binary data port

  my_tios.c_cflag |= CLOCAL;    // port is local, no flow control
  my_tios.c_cflag &= ~CSIZE;
  my_tios.c_cflag |= CS8;       // 8 bit chars
  my_tios.c_cflag &= ~PARENB;   // no parity
  my_tios.c_cflag &= ~CSTOPB;   // 1 stop bit
  my_tios.c_cflag &= ~CRTSCTS;  // no CTS/RTS flow control
  cfsetispeed(&my_tios, B230400);
  cfsetospeed(&my_tios, B230400);

  if (tcsetattr(m_fd, TCSANOW, &my_tios) < 0) {  // apply settings
    OLA_WARN << "Failed to get POSIX port settings";
    return false;
  }

  {
    unsigned char buffer[2];

    buffer[0] = 'Q';
    buffer[1] = 'B';

    if (write(m_fd, buffer, 2) <= 0) {
      // TODO(richardash1981): handle errors better as per the test code,
      // especially if we alter the scheduling!
      OLA_WARN << Name() << " Short or failed write!";
      return false;
    }
  }

  /* everything must have worked to get here */
  return true;
}

}  // namespace harvestdmx
}  // namespace plugin
}  // namespace ola
