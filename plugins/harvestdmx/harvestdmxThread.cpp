/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * harvestdmxThread.cpp
 * The DMX through an Arduino plugin for ola
 * Copyright (C) 2011 Rui Barreiros
 * Copyright (C) 2014 Richard Ash
 * Copyright (C) 2014 David Kuder
 */

#include <math.h>
#include <unistd.h>
#include <string>
#include "ola/Clock.h"
#include "ola/Logging.h"
#include "ola/StringUtils.h"
#include "plugins/harvestdmx/harvestdmxWidget.h"
#include "plugins/harvestdmx/harvestdmxThread.h"

namespace ola {
namespace plugin {
namespace harvestdmx {

harvestdmxThread::harvestdmxThread(harvestdmxWidget *widget, unsigned int channels,
                             unsigned int malft)
  : m_granularity(UNKNOWN),
    m_widget(widget),
    m_term(false),
    m_channels(channels),
    m_malft(malft) {
}

harvestdmxThread::~harvestdmxThread() {
  Stop();
}


/**
 * Stop this thread
 */
bool harvestdmxThread::Stop() {
  {
    ola::thread::MutexLocker locker(&m_term_mutex);
    m_term = true;
  }
  return Join();
}


/**
 * Copy a DMXBuffer to the output thread
 */
bool harvestdmxThread::WriteDMX(const DmxBuffer &buffer) {
  ola::thread::MutexLocker locker(&m_buffer_mutex);
  m_buffer.Set(buffer);
  return true;
}


/**
 * The method called by the thread
 */
void *harvestdmxThread::Run() {
  TimeStamp ts1, ts2;
  Clock clock;
  CheckTimeGranularity();
  DmxBuffer buffer;

  // Setup the widget
  if (!m_widget->IsOpen())
    m_widget->SetupOutput();

  m_widget->SetMax(m_channels);

  while (1) {
    {
      ola::thread::MutexLocker locker(&m_term_mutex);
      if (m_term)
        break;
    }

    {
      ola::thread::MutexLocker locker(&m_buffer_mutex);
      buffer.Set(m_buffer);
    }

    if (!m_widget->Write(buffer))
      goto framesleep;

  framesleep:
    // Sleep for the remainder of the DMX frame time
    usleep(m_malft);
  }
  return NULL;
}


/**
 * Check the granularity of usleep.
 */
void harvestdmxThread::CheckTimeGranularity() {
  TimeStamp ts1, ts2;
  Clock clock;
  /** If sleeping for 1ms takes longer than this, don't trust
   * usleep for this session
   */
  const int threshold = 3;

  clock.CurrentTime(&ts1);
  usleep(1000);
  clock.CurrentTime(&ts2);

  TimeInterval interval = ts2 - ts1;
  m_granularity = interval.InMilliSeconds() > threshold ? BAD : GOOD;
  OLA_INFO << "Granularity for HarvestDMX thread is "
           << (m_granularity == GOOD ? "GOOD" : "BAD");
}
}  // namespace harvestdmx
}  // namespace plugin
}  // namespace ola
