/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * harvestdmxDevice.cpp
 * The DMX through a UART plugin for ola
 * Copyright (C) 2011 Rui Barreiros
 * Copyright (C) 2014 Richard Ash
 */

#include <string>
#include <memory>
#include "ola/Logging.h"
#include "ola/StringUtils.h"
#include "plugins/harvestdmx/harvestdmxDevice.h"
#include "plugins/harvestdmx/harvestdmxPort.h"

namespace ola {
namespace plugin {
namespace harvestdmx {

using std::string;

const char harvestdmxDevice::K_MALF[] = "-malf";
const char harvestdmxDevice::K_CHANNELS[] = "-channels";
const unsigned int harvestdmxDevice::DEFAULT_CHANNELS = 64;
const unsigned int harvestdmxDevice::DEFAULT_MALF = 100;


harvestdmxDevice::harvestdmxDevice(AbstractPlugin *owner,
                             class Preferences *preferences,
                             const string &name,
                             const string &path)
    : Device(owner, name),
      m_preferences(preferences),
      m_name(name),
      m_path(path) {
  // set up some per-device default configuration if not already set
  SetDefaults();
  // now read per-device configuration
  // Break time in microseconds
  if (!StringToInt(m_preferences->GetValue(DeviceChannelsKey()), &m_channels))
    m_channels = DEFAULT_CHANNELS;
  // Mark After Last Frame in microseconds
  if (!StringToInt(m_preferences->GetValue(DeviceMalfKey()), &m_malft))
    m_malft = DEFAULT_MALF;
  m_widget.reset(new harvestdmxWidget(path));
}

harvestdmxDevice::~harvestdmxDevice() {
  if (m_widget->IsOpen())
    m_widget->Close();
}

bool harvestdmxDevice::StartHook() {
  AddPort(new harvestdmxOutputPort(this, 0, m_widget.get(), m_channels, m_malft));
  return true;
}

string harvestdmxDevice::DeviceMalfKey() const {
  return m_path + K_MALF;
}
string harvestdmxDevice::DeviceChannelsKey() const {
  return m_path + K_CHANNELS;
}

/**
 * Set the default preferences for this one Device
 */
void harvestdmxDevice::SetDefaults() {
  bool save = false;

  save |= m_preferences->SetDefaultValue(DeviceChannelsKey(),
                                         UIntValidator(1, 512),
                                         DEFAULT_CHANNELS);
  save |= m_preferences->SetDefaultValue(DeviceMalfKey(),
                                         UIntValidator(8, 1000000),
                                         DEFAULT_MALF);
  if (save)
    m_preferences->Save();
}
}  // namespace harvestdmx
}  // namespace plugin
}  // namespace ola
