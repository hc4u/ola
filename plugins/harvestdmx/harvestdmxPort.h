/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * harvestdmxPort.h
 * The DMX through an Arduino plugin for ola
 * Copyright (C) 2011 Rui Barreiros
 * Copyright (C) 2014 Richard Ash
 * Copyright (C) 2014 David Kuder
 */

#ifndef PLUGINS_HARVESTDMX_HARVESTDMXPORT_H_
#define PLUGINS_HARVESTDMX_HARVESTDMXPORT_H_

#include <string>

#include "ola/DmxBuffer.h"
#include "olad/Port.h"
#include "olad/Preferences.h"
#include "plugins/harvestdmx/harvestdmxDevice.h"
#include "plugins/harvestdmx/harvestdmxWidget.h"
#include "plugins/harvestdmx/harvestdmxThread.h"

namespace ola {
namespace plugin {
namespace harvestdmx {

class harvestdmxOutputPort : public ola::BasicOutputPort {
 public:
  harvestdmxOutputPort(harvestdmxDevice *parent,
                    unsigned int id,
                    harvestdmxWidget *widget,
                    unsigned int channels,
                    unsigned int malft)
      : BasicOutputPort(parent, id),
        m_widget(widget),
        m_thread(widget, channels, malft) {
    m_thread.Start();
  }
  ~harvestdmxOutputPort() { m_thread.Stop(); }

  bool WriteDMX(const ola::DmxBuffer &buffer, uint8_t) {
    return m_thread.WriteDMX(buffer);
  }

  std::string Description() const { return m_widget->Description(); }

 private:
  harvestdmxWidget *m_widget;
  harvestdmxThread m_thread;

  DISALLOW_COPY_AND_ASSIGN(harvestdmxOutputPort);
};
}  // namespace harvestdmx
}  // namespace plugin
}  // namespace ola
#endif  // PLUGINS_HARVESTDMX_HARVESTDMXPORT_H_
