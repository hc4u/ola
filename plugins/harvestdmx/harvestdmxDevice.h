/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * harvestdmxDevice.h
 * The DMX through a UART plugin for ola
 * Copyright (C) 2011 Rui Barreiros
 * Copyright (C) 2014 Richard Ash
 */

#ifndef PLUGINS_HARVESTDMX_HARVESTDMXDEVICE_H_
#define PLUGINS_HARVESTDMX_HARVESTDMXDEVICE_H_

#include <string>
#include <sstream>
#include <memory>
#include "ola/DmxBuffer.h"
#include "olad/Device.h"
#include "olad/Preferences.h"
#include "plugins/harvestdmx/harvestdmxWidget.h"

namespace ola {
namespace plugin {
namespace harvestdmx {

class harvestdmxDevice : public Device {
 public:
  harvestdmxDevice(AbstractPlugin *owner,
                class Preferences *preferences,
                const std::string &name,
                const std::string &path);
  ~harvestdmxDevice();

  std::string DeviceId() const { return m_path; }
  harvestdmxWidget* GetWidget() { return m_widget.get(); }

 protected:
  bool StartHook();

 private:
  // Per device options
  std::string DeviceChannelsKey() const;
  std::string DeviceMalfKey() const;
  void SetDefaults();

  std::auto_ptr<harvestdmxWidget> m_widget;
  class Preferences *m_preferences;
  const std::string m_name;
  const std::string m_path;
  unsigned int m_channels;
  unsigned int m_malft;

  static const unsigned int DEFAULT_MALF;
  static const char K_MALF[];
  static const unsigned int DEFAULT_CHANNELS;
  static const char K_CHANNELS[];

  DISALLOW_COPY_AND_ASSIGN(harvestdmxDevice);
};
}  // namespace harvestdmx
}  // namespace plugin
}  // namespace ola
#endif  // PLUGINS_HARVESTDMX_HARVESTDMXDEVICE_H_
