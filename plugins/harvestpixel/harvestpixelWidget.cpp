/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * This class is based on QLCFTDI class from
 *
 * Q Light Controller
 * qlcftdi-libftdi.cpp
 *
 * Copyright (C) Heikki Junnila
 *
 * Only standard CPP conversion was changed and function name changed
 * to follow OLA coding standards.
 *
 * by Rui Barreiros
 * Copyright (C) 2014 Richard Ash
 */

#include <strings.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
//#include <stropts.h>
#include <fcntl.h>
#include <termios.h>
//#include <asm/termios.h>
#include <unistd.h>

#include <string>
#include <algorithm>
#include <vector>

#include "ola/Constants.h"
#include "ola/io/ExtendedSerial.h"
#include "ola/io/IOUtils.h"
#include "ola/Logging.h"
#include "plugins/harvestpixel/harvestpixelWidget.h"

#include "plugins/harvestpixel/gobo/Barber256.h"
#include "plugins/harvestpixel/gobo/Barber128.h"
#include "plugins/harvestpixel/gobo/Barber64.h"
#include "plugins/harvestpixel/gobo/Barber32.h"
#include "plugins/harvestpixel/gobo/ShimmerBright.h"
#include "plugins/harvestpixel/gobo/ShimmerNormal.h"
#include "plugins/harvestpixel/gobo/ShimmerMedium.h"
#include "plugins/harvestpixel/gobo/ShimmerDark.h"

namespace ola {
namespace plugin {
namespace harvestpixel {

using std::string;
using std::vector;

harvestpixelWidget::harvestpixelWidget(const string& path)
    : m_path(path),
      m_fd(NOT_OPEN) {
  m_frameno = 0;
}

harvestpixelWidget::~harvestpixelWidget() {
  if (IsOpen())
    Close();
}

bool harvestpixelWidget::Open() {
  OLA_DEBUG << "Opening serial port " << Name();
  if (!ola::io::Open(m_path, O_WRONLY, &m_fd)) {
    m_fd = FAILED_OPEN;
    OLA_WARN << Name() << " failed to open";
    return false;
  } else {
    OLA_DEBUG << "Opened serial port " << Name();
    return true;
  }
}

bool harvestpixelWidget::Close() {
  if (!IsOpen()) {
    return true;
  }

  if (close(m_fd) > 0) {
    OLA_WARN << Name() << " error closing";
    m_fd = NOT_OPEN;
    return false;
  } else {
    m_fd = NOT_OPEN;
    return true;
  }
}

bool harvestpixelWidget::IsOpen() const {
  return m_fd >= 0;
}

bool harvestpixelWidget::SetBreak(bool on) {
  (void)on;
  // Do nothing...
  return true;
}

bool harvestpixelWidget::SetAddress(const int dmxAddress) {
  m_channel = dmxAddress;
  return true;
}

bool harvestpixelWidget::SetLines(const int dmxLines) {
  m_lines = dmxLines;
  return true;
}

bool harvestpixelWidget::SetPixels(const int dmxPixels) {
  m_pixels = dmxPixels;
  return true;
}

#define ABITS 4
#define HSCALE 256

typedef struct col32bit {
    union {
        uint8_t r;
        uint8_t h;
    };
    union {
        uint8_t g;
        uint8_t s;
    };
    union {
        uint8_t b;
        uint8_t v;
    };
    uint8_t a;
} col32bit_t;

typedef struct col64bit {
    union {
        uint16_t r;
        uint16_t h;
    };
    union {
        uint16_t g;
        uint16_t s;
    };
    union {
        uint16_t b;
        uint16_t v;
    };
    uint16_t a;
} col64bit_t;

inline col64bit rgba2hsva(const col32bit c){
  col64bit r;
  uint32_t iMin,iMax,chroma;
  const uint32_t k1=255 << ABITS;
  const uint32_t k2=HSCALE << ABITS;
  
  // there's no need to touch alpha, it remains 8-bit
  r.a=c.a;

  if (c.r > c.g) {
      iMax = std::max (c.r, c.b);
      iMin = std::min (c.g, c.b);
  } else {
      iMax = std::max (c.g, c.b);
      iMin = std::min (c.r, c.b);
  }

  chroma = iMax - iMin;
  // set value
  r.v = iMax << ABITS;

  // set saturation
  if (r.v == 0)
    r.s = 0;
  else
    r.s = (k1*chroma)/iMax;

  // set hue 
  if (r.s == 0)
      r.h = 0;
  else {
      if ( c.r == iMax ) {
        r.h  =  (k2*(6*chroma+c.g - c.b))/(6*chroma);
        if (r.h >= k2) r.h -= k2;
      } else if (c.g  == iMax)
        r.h  =  (k2*(2*chroma+c.b - c.r )) / (6*chroma);
      else // (c.b == iMax )
        r.h  =  (k2*(4*chroma+c.r - c.g )) / (6*chroma);
  }
  return r;
}

inline col32bit hsva2rgba(const col64bit c){
  col32bit r;
  uint32_t m;
  int32_t H,X,ih,is,iv;
  const uint32_t k1=255 << ABITS;
  const int32_t k2=HSCALE << ABITS;
  const int32_t k3=1<<(ABITS-1);
  r.a=c.a;

  // set chroma and min component value m
  //chroma = ( c.v * c.s )/k1;
  //m = c.v - chroma;
  m = ((uint32_t)c.v*(k1 - (uint32_t )c.s ))/k1;

  // chroma  == 0 <-> c.s == 0 --> m=c.v
  if (c.s == 0) {
      r.b = ( r.g = ( r.r = c.v >> ABITS ));
  } else {
    ih=(int32_t)c.h;
    is=(int32_t)c.s;
    iv=(int32_t)c.v;

    H = (6*ih)/k2;
    X = ((iv*is)/k2)*(k2-abs(6*ih- 2*(H>>1)*k2 - k2)) ;

    // removing additional bits --> unit8
    X=((X+iv*(k1 - is))/k1 + k3) >>ABITS;
    m=m >> ABITS;

    // ( chroma + m ) --> c.v ;
    switch (H) {
        case 0:
          r.r = c.v >> ABITS ;
          r.g = X;
          r.b = m ;
          break;
        case 1:
          r.r = X;
          r.g = c.v >> ABITS;
          r.b = m ;
          break;
        case 2:
          r.r = m ;
          r.g = c.v >> ABITS;
          r.b = X;
          break;
        case 3:
          r.r = m ;
          r.g = X;
          r.b = c.v >> ABITS;
          break;
        case 4:
          r.r = X;
          r.g = m ;
          r.b = c.v >> ABITS;
          break;
        case 5:
          r.r = c.v >> ABITS;
          r.g = m ;
          r.b = X;
          break;
    }
  }
  return r;
}

inline int16_t u2s8(int16_t u) {
  if(u > 127) {
    return u - 255;
  } else {
    return u;
  }
}

bool harvestpixelWidget::Write(const ola::DmxBuffer& data) {
  unsigned char ibuffer[DMX_UNIVERSE_SIZE];
  unsigned char *obuffer =
    (unsigned char *)malloc((m_lines * m_pixels * 3) + 6);
  unsigned int length = m_channel + m_lines * 16;
  int xLine, xPixel, xTotal;

  data.Get(ibuffer, &length);

  if(length > 0) {
    xTotal = m_lines * m_pixels;

    obuffer[0] = 'A';
    obuffer[1] = 'd';
    obuffer[2] = 'a';
    obuffer[3] = (xTotal - 1) >> 8;
    obuffer[4] = (xTotal - 1) & 0xff;
    obuffer[5] = obuffer[3] ^ obuffer[4] ^ 0x55;

    // Nybblize the dmx data for transmission.
    for(xLine = 0; xLine < m_lines; xLine++) {
      col32bit sColor;
      col64bit iColor;

      // Initial Color
      sColor.r = ibuffer[m_channel + xLine * 16 + 0];
      sColor.g = ibuffer[m_channel + xLine * 16 + 1];
      sColor.b = ibuffer[m_channel + xLine * 16 + 2];

      // HSV Deltas (signed 16-bit integers)
      int16_t dH = (ibuffer[m_channel + xLine * 16 + 3] << 8) |
                   ibuffer[m_channel + xLine * 16 + 4];
      int16_t dS = (ibuffer[m_channel + xLine * 16 + 5] << 8) |
                   ibuffer[m_channel + xLine * 16 + 6];
      int16_t dV = (ibuffer[m_channel + xLine * 16 + 7] << 8) |
                   ibuffer[m_channel + xLine * 16 + 8];

      // Overall Dimming
      int32_t sD = (int32_t)((unsigned char)(ibuffer[m_channel + xLine * 16 + 9]));
      if(sD > 0) sD++;

      // Pixel Shift (Color Rotation)
      int16_t sP = ((ibuffer[m_channel + xLine * 16 + 10] & 0xff) << 8) |
                   (ibuffer[m_channel + xLine * 16 + 11] & 0xff);

      int PixelPosition = (sP * m_frameno) >> 8;

      // Gobo Patterns
      unsigned char gPattern = ibuffer[m_channel + xLine * 16 + 12];
      unsigned char gStart = ibuffer[m_channel + xLine * 16 + 13];
      unsigned char gStop = ibuffer[m_channel + xLine * 16 + 14];
      unsigned char gSpeed = ibuffer[m_channel + xLine * 16 + 15];

      int GoboPosition = ((gSpeed * m_frameno) >> 4) & 0x3ff;

      // Convert initial color to HSV
      iColor = rgba2hsva(sColor);

      // Starting Pixel
      iColor.h = (iColor.h + dH * PixelPosition) & 0xfff;
      iColor.s = (iColor.s + dS * PixelPosition) & 0xfff;
      iColor.v = (iColor.v + dV * PixelPosition) & 0xfff;

      for(xPixel = 0; xPixel < m_pixels; xPixel++) {
	// Per-Pixel Color Shifts
        iColor.h = (iColor.h + dH) & 0xfff;
        iColor.s = (iColor.s + dS) & 0xfff;
        iColor.v = (iColor.v + dV) & 0xfff;

	// Convert back to RGB
        sColor = hsva2rgba(iColor);

	// Get ready to apply Effects and Dimmer value
        int32_t iR = ((int32_t)sColor.r);
        int32_t iG = ((int32_t)sColor.g);
        int32_t iB = ((int32_t)sColor.b);

        int GoboPixel = 255;

        switch(gPattern) {
          // case 0: break; // No Pattern
          case 1:
            GoboPixel = Barber32[256 * GoboPosition + xPixel];
            break;
          case 2:
            GoboPixel = Barber32[256 * GoboPosition + 255 - xPixel];
            break;
          case 3:
            GoboPixel = Barber64[256 * GoboPosition + xPixel];
            break;
          case 4:
            GoboPixel = Barber64[256 * GoboPosition + 255 - xPixel];
            break;
          case 5:
            GoboPixel = Barber128[256 * GoboPosition + xPixel];
            break;
          case 6:
            GoboPixel = Barber128[256 * GoboPosition + 255 - xPixel];
            break;
          case 7:
            GoboPixel = Barber256[256 * GoboPosition + xPixel];
            break;
          case 8:
            GoboPixel = Barber256[256 * GoboPosition + 255 - xPixel];
            break;
          case 9:
            GoboPixel = ShimmerDark[256 * GoboPosition + xPixel];
            break;
          case 10:
            GoboPixel = ShimmerDark[256 * GoboPosition + 255 - xPixel];
            break;
          case 11:
            GoboPixel = ShimmerMedium[256 * GoboPosition + xPixel];
            break;
          case 12:
            GoboPixel = ShimmerMedium[256 * GoboPosition + 255 - xPixel];
            break;
          case 13:
            GoboPixel = ShimmerNormal[256 * GoboPosition + xPixel];
            break;
          case 14:
            GoboPixel = ShimmerNormal[256 * GoboPosition + 255 - xPixel];
            break;
          case 15:
            GoboPixel = ShimmerBright[256 * GoboPosition + xPixel];
            break;
          case 16:
            GoboPixel = ShimmerBright[256 * GoboPosition + 255 - xPixel];
            break;
        };

	if(GoboPixel > 0) GoboPixel++;
	if(gStart < gStop) {
		if((xPixel < gStart) ||
		  (xPixel > gStop)) GoboPixel = 0;
	} else if(gStop < gStart) {
		if((xPixel < gStart) &&
		  (xPixel > gStop)) GoboPixel = 0;
	}

        iR = iR * sD * GoboPixel;
        iG = iG * sD * GoboPixel;
        iB = iB * sD * GoboPixel;

        obuffer[6 + (xLine * m_pixels + xPixel) * 3 + 0] = iR >> 16;
        obuffer[6 + (xLine * m_pixels + xPixel) * 3 + 1] = iG >> 16;
        obuffer[6 + (xLine * m_pixels + xPixel) * 3 + 2] = iB >> 16;
      }
    }

    m_frameno++;

    if (write(m_fd, obuffer, xTotal*3 + 6) <= 0) {
      // TODO(richardash1981): handle errors better as per the test code,
      // especially if we alter the scheduling!
      OLA_WARN << Name() << " Short or failed write!";
      free(obuffer);
      return false;
    }
  }

  free(obuffer);

  return true;
}

bool harvestpixelWidget::Read(unsigned char *buff, int size) {
  int readb = read(m_fd, buff, size);
  if (readb <= 0) {
    OLA_WARN << Name() << " read error";
    return false;
  } else {
    return true;
  }
}

/**
 * Setup our device for send
 * Also used to test if device is working correctly
 * before AddDevice()
 */
bool harvestpixelWidget::SetupOutput() {
  struct termios my_tios;

  // Setup the Arduino
  if (Open() == false) {
    OLA_WARN << "Error Opening widget";
    return false;
  }

  /* do the port settings */

  if (tcgetattr(m_fd, &my_tios) < 0) {  // get current settings
    OLA_WARN << "Failed to get POSIX port settings";
    return false;
  }
  cfmakeraw(&my_tios);  // make it a binary data port

  my_tios.c_cflag |= CLOCAL;    // port is local, no flow control
  my_tios.c_cflag &= ~CSIZE;
  my_tios.c_cflag |= CS8;       // 8 bit chars
  my_tios.c_cflag &= ~PARENB;   // no parity
  my_tios.c_cflag &= ~CSTOPB;   // 1 stop bit
  my_tios.c_cflag &= ~CRTSCTS;  // no CTS/RTS flow control
  cfsetispeed(&my_tios, B230400);
  cfsetospeed(&my_tios, B230400);

  if (tcsetattr(m_fd, TCSANOW, &my_tios) < 0) {  // apply settings
    OLA_WARN << "Failed to set POSIX port settings";
    return false;
  }

  /* everything must have worked to get here */
  return true;
}

}  // namespace harvestpixel
}  // namespace plugin
}  // namespace ola
