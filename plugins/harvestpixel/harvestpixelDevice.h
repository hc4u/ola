/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * harvestpixelDevice.h
 * The DMX through a UART plugin for ola
 * Copyright (C) 2011 Rui Barreiros
 * Copyright (C) 2014 Richard Ash
 */

#ifndef PLUGINS_HARVESTPIXEL_HARVESTPIXELDEVICE_H_
#define PLUGINS_HARVESTPIXEL_HARVESTPIXELDEVICE_H_

#include <string>
#include <sstream>
#include <memory>
#include "ola/DmxBuffer.h"
#include "olad/Device.h"
#include "olad/Preferences.h"
#include "plugins/harvestpixel/harvestpixelWidget.h"

namespace ola {
namespace plugin {
namespace harvestpixel {

class harvestpixelDevice : public Device {
 public:
  harvestpixelDevice(AbstractPlugin *owner,
                class Preferences *preferences,
                const std::string &name,
                const std::string &path);
  ~harvestpixelDevice();

  std::string DeviceId() const { return m_path; }
  harvestpixelWidget* GetWidget() { return m_widget.get(); }

 protected:
  bool StartHook();

 private:
  // Per device options
  std::string DeviceChannelKey() const;
  std::string DeviceLinesKey() const;
  std::string DevicePixelsKey() const;
  std::string DeviceMalfKey() const;
  void SetDefaults();

  std::auto_ptr<harvestpixelWidget> m_widget;
  class Preferences *m_preferences;
  const std::string m_name;
  const std::string m_path;
  unsigned int m_channel;
  unsigned int m_lines;
  unsigned int m_pixels;
  unsigned int m_malft;

  static const unsigned int DEFAULT_MALF;
  static const char K_MALF[];
  static const unsigned int DEFAULT_CHANNEL;
  static const char K_CHANNEL[];
  static const unsigned int DEFAULT_LINES;
  static const char K_LINES[];
  static const unsigned int DEFAULT_PIXELS;
  static const char K_PIXELS[];

  DISALLOW_COPY_AND_ASSIGN(harvestpixelDevice);
};
}  // namespace harvestpixel
}  // namespace plugin
}  // namespace ola
#endif  // PLUGINS_HARVESTPIXEL_HARVESTPIXELDEVICE_H_
