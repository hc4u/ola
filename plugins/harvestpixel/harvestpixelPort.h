/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * harvestpixelPort.h
 * The DMX through an Arduino plugin for ola
 * Copyright (C) 2011 Rui Barreiros
 * Copyright (C) 2014 Richard Ash
 * Copyright (C) 2014 David Kuder
 */

#ifndef PLUGINS_HARVESTPIXEL_HARVESTPIXELPORT_H_
#define PLUGINS_HARVESTPIXEL_HARVESTPIXELPORT_H_

#include <string>

#include "ola/DmxBuffer.h"
#include "olad/Port.h"
#include "olad/Preferences.h"
#include "plugins/harvestpixel/harvestpixelDevice.h"
#include "plugins/harvestpixel/harvestpixelWidget.h"
#include "plugins/harvestpixel/harvestpixelThread.h"

namespace ola {
namespace plugin {
namespace harvestpixel {

class harvestpixelOutputPort : public ola::BasicOutputPort {
 public:
  harvestpixelOutputPort(harvestpixelDevice *parent,
                    unsigned int id,
                    harvestpixelWidget *widget,
                    unsigned int channel,
                    unsigned int lines,
                    unsigned int pixels,
                    unsigned int malft)
      : BasicOutputPort(parent, id),
        m_widget(widget),
        m_thread(widget, channel, lines, pixels, malft) {
    m_thread.Start();
  }
  ~harvestpixelOutputPort() { m_thread.Stop(); }

  bool WriteDMX(const ola::DmxBuffer &buffer, uint8_t) {
    return m_thread.WriteDMX(buffer);
  }

  std::string Description() const { return m_widget->Description(); }

 private:
  harvestpixelWidget *m_widget;
  harvestpixelThread m_thread;

  DISALLOW_COPY_AND_ASSIGN(harvestpixelOutputPort);
};
}  // namespace harvestpixel
}  // namespace plugin
}  // namespace ola
#endif  // PLUGINS_HARVESTPIXEL_HARVESTPIXELPORT_H_
