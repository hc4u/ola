# LIBRARIES
##################################################
if USE_HARVESTPIXEL
lib_LTLIBRARIES += plugins/harvestpixel/libolaharvestpixel.la
plugins_harvestpixel_libolaharvestpixel_la_SOURCES = \
    plugins/harvestpixel/harvestpixelDevice.cpp \
    plugins/harvestpixel/harvestpixelDevice.h \
    plugins/harvestpixel/harvestpixelPlugin.cpp \
    plugins/harvestpixel/harvestpixelPlugin.h \
    plugins/harvestpixel/harvestpixelPort.h \
    plugins/harvestpixel/harvestpixelThread.cpp \
    plugins/harvestpixel/harvestpixelThread.h \
    plugins/harvestpixel/harvestpixelWidget.cpp \
    plugins/harvestpixel/harvestpixelWidget.h
plugins_harvestpixel_libolaharvestpixel_la_LIBADD = common/libolacommon.la
endif

