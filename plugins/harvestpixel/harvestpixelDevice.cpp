/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * harvestpixelDevice.cpp
 * The DMX through a UART plugin for ola
 * Copyright (C) 2011 Rui Barreiros
 * Copyright (C) 2014 Richard Ash
 */

#include <string>
#include <memory>
#include "ola/Logging.h"
#include "ola/StringUtils.h"
#include "plugins/harvestpixel/harvestpixelDevice.h"
#include "plugins/harvestpixel/harvestpixelPort.h"

namespace ola {
namespace plugin {
namespace harvestpixel {

using std::string;

const char harvestpixelDevice::K_MALF[] = "-malf";
const char harvestpixelDevice::K_LINES[] = "-lines";
const char harvestpixelDevice::K_PIXELS[] = "-pixels";
const char harvestpixelDevice::K_CHANNEL[] = "-channel";
const unsigned int harvestpixelDevice::DEFAULT_CHANNEL = 0;
const unsigned int harvestpixelDevice::DEFAULT_LINES = 8;
const unsigned int harvestpixelDevice::DEFAULT_PIXELS = 60;
const unsigned int harvestpixelDevice::DEFAULT_MALF = 100;

harvestpixelDevice::harvestpixelDevice(AbstractPlugin *owner,
                             class Preferences *preferences,
                             const string &name,
                             const string &path)
    : Device(owner, name),
      m_preferences(preferences),
      m_name(name),
      m_path(path) {

  // set up some per-device default configuration if not already set
  SetDefaults();

  // now read per-device configuration

  // Channel address of fixture
  if (!StringToInt(m_preferences->GetValue(DeviceChannelKey()), &m_channel))
    m_channel = DEFAULT_CHANNEL;

  // Number of Pixel Rows
  if (!StringToInt(m_preferences->GetValue(DeviceLinesKey()), &m_lines))
    m_lines = DEFAULT_LINES;

  // Number of Pixels per row
  if (!StringToInt(m_preferences->GetValue(DevicePixelsKey()), &m_pixels))
    m_pixels = DEFAULT_PIXELS;

  // Mark After Last Frame in microseconds
  if (!StringToInt(m_preferences->GetValue(DeviceMalfKey()), &m_malft))
    m_malft = DEFAULT_MALF;
  m_widget.reset(new harvestpixelWidget(path));
}

harvestpixelDevice::~harvestpixelDevice() {
  if (m_widget->IsOpen())
    m_widget->Close();
}

bool harvestpixelDevice::StartHook() {
  AddPort(new harvestpixelOutputPort(this, 0, m_widget.get(), m_channel, m_lines, m_pixels, m_malft));
  return true;
}

string harvestpixelDevice::DeviceMalfKey() const {
  return m_path + K_MALF;
}
string harvestpixelDevice::DeviceLinesKey() const {
  return m_path + K_LINES;
}
string harvestpixelDevice::DevicePixelsKey() const {
  return m_path + K_PIXELS;
}
string harvestpixelDevice::DeviceChannelKey() const {
  return m_path + K_CHANNEL;
}

/**
 * Set the default preferences for this one Device
 */
void harvestpixelDevice::SetDefaults() {
  bool save = false;

  save |= m_preferences->SetDefaultValue(DeviceLinesKey(),
                                         UIntValidator(1, 8),
                                         DEFAULT_LINES);
  save |= m_preferences->SetDefaultValue(DevicePixelsKey(),
                                         UIntValidator(1, 500),
                                         DEFAULT_PIXELS);
  save |= m_preferences->SetDefaultValue(DeviceChannelKey(),
                                         UIntValidator(0, 500),
                                         DEFAULT_CHANNEL);
  save |= m_preferences->SetDefaultValue(DeviceMalfKey(),
                                         UIntValidator(8, 1000000),
                                         DEFAULT_MALF);
  if (save)
    m_preferences->Save();
}
}  // namespace harvestpixel
}  // namespace plugin
}  // namespace ola
