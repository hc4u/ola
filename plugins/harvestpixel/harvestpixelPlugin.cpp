/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * harvestpixelPlugin.cpp
 * The DMX through an Arduino plugin for ola
 * Copyright (C) 2011 Rui Barreiros
 * Copyright (C) 2014 Richard Ash
 * Copyright (C) 2014 David Kuder
 */

#include <fcntl.h>
#include <errno.h>

#include <memory>
#include <string>
#include <vector>

#include "ola/StringUtils.h"
#include "ola/io/IOUtils.h"
#include "olad/Preferences.h"
#include "olad/PluginAdaptor.h"
#include "plugins/harvestpixel/harvestpixelPlugin.h"
#include "plugins/harvestpixel/harvestpixelDevice.h"
#include "plugins/harvestpixel/harvestpixelWidget.h"

namespace ola {
namespace plugin {
namespace harvestpixel {

using std::string;
using std::vector;

const char harvestpixelPlugin::PLUGIN_NAME[] = "Harvest Pixel";
const char harvestpixelPlugin::PLUGIN_PREFIX[] = "harvestpixel";
const char harvestpixelPlugin::K_DEVICE[] = "device";
const char harvestpixelPlugin::DEFAULT_DEVICE[] = "/dev/ttyACM0";

/*
 * Start the plug-in, using only the configured device(s) (we cannot sensibly
 * scan for UARTs!). Stolen from the opendmx plugin.
 */
bool harvestpixelPlugin::StartHook() {
  vector<string> devices = m_preferences->GetMultipleValue(K_DEVICE);
  vector<string>::const_iterator iter;  // iterate over devices

  // start counting device ids from 0

  for (iter = devices.begin(); iter != devices.end(); ++iter) {
    // first check if device configured
    if (iter->empty()) {
      OLA_DEBUG << "No path configured for device, please set one in "
          "ola-harvestpixel.conf";
      continue;
    }

    OLA_DEBUG << "Trying to open UART device " << *iter;
    int fd;
    if (!ola::io::Open(*iter, O_WRONLY, &fd)) {
      OLA_WARN << "Could not open " << *iter << " " << strerror(errno);
      continue;
    }

    // can open device, so shut the temporary file descriptor
    close(fd);
    std::auto_ptr<harvestpixelDevice> device(new harvestpixelDevice(
        this, m_preferences, PLUGIN_NAME, *iter));

    // got a device, now lets see if we can configure it before we announce
    // it to the world
    if (!device->GetWidget()->SetupOutput()) {
      OLA_WARN << "Unable to setup device for output, device ignored "
               << device->DeviceId();
      continue;
    }
    // OK, device is good to go
    if (!device->Start()) {
      OLA_WARN << "Failed to start harvestpixelDevice for " << *iter;
      continue;
    }

    OLA_DEBUG << "Started harvestpixelDevice " << *iter;
    m_plugin_adaptor->RegisterDevice(device.get());
    m_devices.push_back(device.release());
  }
  return true;
}

/**
 * Stop all the devices.
 */
bool harvestpixelPlugin::StopHook() {
  harvestpixelDeviceVector::iterator iter;
  for (iter = m_devices.begin(); iter != m_devices.end(); ++iter) {
    m_plugin_adaptor->UnregisterDevice(*iter);
    (*iter)->Stop();
    delete *iter;
  }
  m_devices.clear();
  return true;
}


/**
 * Return a description for this plugin.
 */
string harvestpixelPlugin::Description() const {
  return
"Harvest Pixel Plugin\n"
"----------------------\n"
"\n"
"This plugin drives a Teensy 3.1 running the Adalight firmware\n"
"to produce a WS2812 output stream.\n"
"\n"
"--- Config file : ola-harvestpixel.conf ---\n"
"\n"
"enabled = true\n"
"Enable this plugin (DISABLED by default).\n"
"device = /dev/ttyACM0\n"
"The device to use for output (optional). Multiple devices are supported "
"if the hardware exists.\n"
"--- Per Device Settings (using above device name without /dev/) ---\n"
"<device>-channel = 1\n"
"The first DMX channel assignment.\n"
"<device>-lines = 8\n"
"The number of WS2812 lines.\n"
"<device>-pixels = 60\n"
"The number of WS2812 pixles per line.\n"
"<device>-malf = 100\n"
"Delay between frames.\n"
"\n";
}


/**
 * Set the default preferences
 */
bool harvestpixelPlugin::SetDefaultPreferences() {
  if (!m_preferences)
    return false;

  // only insert default device name, no others at this stage
  bool save = m_preferences->SetDefaultValue(K_DEVICE, StringValidator(),
                                             DEFAULT_DEVICE);
  if (save)
    m_preferences->Save();

  // Just check key exists, as we've set it to ""
  if (!m_preferences->HasKey(K_DEVICE))
    return false;
  return true;
}
}  // namespace harvestpixel
}  // namespace plugin
}  // namespace ola
